# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from work_halaman_profile.models import Profile

# Create your views here.
response = {}
def index(request):
    response['author'] = "Stanley Sebastian" #TODO Implement yourname
    status = Status.objects.all()
    response['form'] = Status_Form
    response['model'] = status
    response['title'] = 'Update Status'
    html = 'work_update_status/work_update_status.html'
    response['status_form'] = Status_Form

    if Profile.objects.all().count() == 0:
        Profile.objects.create(name='Ubiquitous', birthday='2017-10-07', gender='Male', expertise='Living life', description='Live life to the fullest', email='ubiquitous@life.com', image='')

    namaProfile = Profile.objects.get(name='Ubiquitous').name
    response['nama'] = namaProfile

    profilePicture = Profile.objects.get(name='Ubiquitous').image
    response['profilepic'] = profilePicture

    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')
