from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_status
from .models import Status
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UpdateStatusUnitTest(TestCase) :

	def test_update_status_url_is_exist(self):
		response = Client().get('/update-status/')
		self.assertEqual(response.status_code, 200)

	def test_updateStatus_using_index_func(self):
		found = resolve('/update-status/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_post(self):
		# Creating a new activity
		new_status = Status.objects.create(description='mengerjakan update_status tugas 1 ppw')

		# Retrieving all available activity
		counting_all_available_post = Status.objects.all().count()
		self.assertEqual(counting_all_available_post, 1)

	def test_form_post_input_has_placeholder_and_css_classes(self):
		form = Status_Form()
		self.assertIn('class="status-form-textarea', form.as_p())
		self.assertIn('id="id_description', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Status_Form(data={'description': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['description'],
			["This field is required."]
		)
	def test_updatestatus_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/update-status/add_status', {'description': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/update-status/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_lab5_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/update-status/add_status', {'description': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/update-status/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)
