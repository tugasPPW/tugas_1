from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    description_attrs = {
        'type': 'text',
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':'Whats on your mind?'
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
