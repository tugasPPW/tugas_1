# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from work_halaman_profile.models import Profile
from work_add_friend.models import Friend
from work_update_status.models import Status

response = {}

# Create your views here.
def index(request):
    response['author'] = "Michael Giorgio"
    html = 'work_statistik/work_statistik.html'

    if Profile.objects.all().count() == 0:
        Profile.objects.create(name='Ubiquitous', birthday='2017-10-07', gender='Male', expertise='Living life', description='Live life to the fullest', email='ubiquitous@life.com', image='')


    profilePicture = Profile.objects.get(name='Ubiquitous').image
    response['profilepic'] = profilePicture

    profileName = Profile.objects.get(name='Ubiquitous').name
    response['name'] = profileName


    numberOfFriends = Friend.objects.all().count()
    response['friendscount'] = numberOfFriends

    numberOfFeeds = Status.objects.all().count()
    response['feedscount'] = numberOfFeeds

    if numberOfFeeds == 0:
        response['feed'] = 'No posts yet!'
        response['feeddate'] = ''
    else:
        latestFeed = Status.objects.get(pk=numberOfFeeds).description
        response['feed'] = latestFeed

        latestFeedTime = Status.objects.get(pk=numberOfFeeds).created_date
        response['feeddate'] = latestFeedTime

    return render(request, html, response)
