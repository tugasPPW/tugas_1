# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from work_halaman_profile.models import Profile
from work_update_status.models import Status

# Create your tests here.
class StatistikUnitTest(TestCase):

    def test_statistik_url_is_exist(self):
        response = Client().get('/statistik/')
        self.assertEqual(response.status_code, 200)

    def test_statistik_using_index_func(self):
        found = resolve('/statistik/')
        self.assertEqual(found.func, index)



    def test_statistik_status_is_exist(self):
        status= Status.objects.create(description="TEST")
        response = Client().get('/statistik/')
        html_response = response.content.decode('utf8')
        self.assertIn("TEST", html_response)
