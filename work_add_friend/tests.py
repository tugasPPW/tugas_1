# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
from .models import Friend
from .forms import FriendURL_Form
from .views import index, friend_post

class addFriend_UnitTest(TestCase) :
	def test_addF_url_is_exist(self):
		response = Client().get('/add-friend/')
		self.assertEqual(response.status_code,200)

	def test_addF_using_index_func(self):
		found = resolve('/add-friend/')
		self.assertEqual(found.func, index)

	#masih harus dikaji ulang, soalnya kalo dilab 4 tuh
	#ada import2 dari kelas lain gitu

	def test_model_can_create_new_friend_and_url(self):
		#Creating a new friend
		new_friend = Friend.objects.create(name='nama', URL='https://g.com')

		#Retrieving all available activity
		counting_all_available_friend= Friend.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)

	def test_form_message_input_has_placeholder_and_css_classes(self):
		form = FriendURL_Form()
		self.assertIn('class="form-control"', form.as_p())
		self.assertIn('<label for="id_name">Nama:</label>', form.as_p())
		self.assertIn('<label for="id_URL">Url:</label>', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = FriendURL_Form(data={'name': '', 'URL': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['URL'],
			["This field is required."]

		)
	def test_user_friends_post_success_and_render_the_result(self):
		test = 'https://hai.herokuapp.com'
		response_post = Client().post('/add-friend/friendAdded', {'name':'test', 'URL':test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/add-friend/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)


#harusnya ada error bila ... yang dimasukkin bukan sebuah url
#jadi ga ada ".com" dan "https://" nya
	def test_addF_post_fail(self):
		response = Client().post('/add-friend/friendAdded', {'name': 'Anonymous', 'URL': ''})
		self.assertEqual(response.status_code, 302)
