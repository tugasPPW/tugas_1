# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .forms import FriendURL_Form
from .models import Friend
from django.http import HttpResponseRedirect

response = {}

# Create your views here.
def index (request):
    response['author'] = "Nabila Laili H"
    html = 'work_add_friend/work_add_friend.html'
    teman = Friend.objects.all()
    response['teman'] = teman
    response['friendurl_form'] = FriendURL_Form
    return render(request, html, response)

def friend_post(request):
	form = FriendURL_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
		response['URL'] = request.POST['URL'] if request.POST['URL'] != "" else "Anonymous"
		teman = Friend(name=response['name'], URL=response['URL'])
		teman.save()
		return HttpResponseRedirect('/add-friend/')
	else:
		return HttpResponseRedirect('/add-friend/')
