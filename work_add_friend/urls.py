from django.conf.urls import url
from .views import index, friend_post
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^friendAdded', friend_post, name='addFriend'),
]
