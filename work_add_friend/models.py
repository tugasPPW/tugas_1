# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Friend(models.Model):
	name = models.CharField(max_length=27)
	URL = models.URLField()
	created_date = models.DateTimeField(auto_now_add=True)