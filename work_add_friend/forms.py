from django import forms

class FriendURL_Form(forms.Form):
	error_messages = {
		'required': 'Tolong isi input ini',
		'invalid': 'Isi input dengan URL',
	}
	attrs = {
		'class': 'form-control'
	}

	name = forms.CharField(label='Nama', max_length=27, required = True, widget=forms.TextInput(attrs=attrs))
	URL = forms.URLField(widget=forms.URLInput(attrs=attrs), required = True)