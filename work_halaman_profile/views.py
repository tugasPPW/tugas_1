# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import Profile

response = {}
#bio_dict = [{'subject' : 'Birthday', 'value' : '01 Jan'},\
#{'subject' : 'Gender', 'value' : 'Female'},\
#{'subject' : 'Expertise', 'value' : 'Marketing'},\
#{'subject' : 'Description', 'value' : 'lalala'},\
#{'subject' : 'Email', 'value' : 'lalaa@gmail.com'}]

# Create your views here.
def index (request):
    # response = {'bio_dict':bio_dict}
    if Profile.objects.all().count() == 0:
        Profile.objects.create(name='Ubiquitous', birthday='2017-10-07', gender='Male', expertise='Living life', description='Live life to the fullest', email='ubiquitous@life.com', image='https://i.pinimg.com/736x/f1/03/c3/f103c3c8b80e7a8cc77958c0811f8853--team-unicorn-unicorns-art-drawing.jpg')

    profile = Profile.objects.get(name = 'Ubiquitous')
    response['author'] = "M. Ikhsan Kurniawan"
    response['profile'] = profile
    html = 'work_halaman_profile/work_halaman_profile.html'
    return render(request, html, response)
