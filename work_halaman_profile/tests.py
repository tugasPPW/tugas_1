from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class HalamanProfileUnitTest(TestCase):
    def test_name_is_exist(self):
        response = Client().get('/halaman-profile/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/halaman-profile/')
        self.assertEqual(found.func, index)

    # def test_all_profile_is_exist(self):
    #     #to check whether name is not none
    #     self.assertIsNotNone(name)
    #     self.assertIsNotNone(birthday)
    #     self.assertIsNotNone(gender)
    #     self.assertIsNotNone(description)
    #     self.assertIsNotNone(expertise)
    #     self.assertIsNotNone(email)
