# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class WorkHalamanProfileConfig(AppConfig):
    name = 'work_halaman_profile'
