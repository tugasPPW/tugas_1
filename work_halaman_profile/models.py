# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Profile(models.Model):
    name = models.CharField(max_length = 100, unique = True)
    birthday = models.DateField()
    gender = models.CharField(max_length = 10)
    expertise = models.CharField(max_length = 100)
    description = models.TextField()
    email = models.EmailField()
    image = models.TextField(default="https://i.pinimg.com/736x/f1/03/c3/f103c3c8b80e7a8cc77958c0811f8853--team-unicorn-unicorns-art-drawing.jpg")