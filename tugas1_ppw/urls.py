from django.conf.urls import include, url
from django.contrib import admin
import work_update_status.urls as work_update_status
import work_halaman_profile.urls as work_halaman_profile
import work_add_friend.urls as work_add_friend
import work_statistik.urls as work_statistik
from django.views.generic import RedirectView

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^update-status/', include(work_update_status,namespace='work_update_status')),
    url(r'^halaman-profile/', include(work_halaman_profile,namespace='work_halaman_profile')),
    url(r'^add-friend/', include(work_add_friend,namespace='work_add_friend')),
    url(r'^statistik/', include(work_statistik,namespace='work_statistik')),
    url(r'^$', RedirectView.as_view(url='/update-status/', permanent = 'true'), name='index')
]
    # Examples:
    # url(r'^$', 'tugas1_ppw.views.home', name='home'),
    # url(r'^tugas1_ppw/', include('tugas1_ppw.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
